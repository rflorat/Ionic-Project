angular.module('app.controllers', [])

.controller('AppCtrl', ['$scope', '$ionicPlatform', '$cordovaSocialSharing', function($scope, $ionicPlatform, $cordovaSocialSharing) {

  $ionicPlatform.ready(function() {

    var message = 'This is a demo message';
    var subject = 'This is a demo message';
    var link = 'img/corso-pilates.jpg'; // fake image

    $scope.nativeShare = function() {
      $cordovaSocialSharing
        .share(message, subject, link); // Share via native share sheet
    };
  });

}])

.controller('SediCtrl', ['$scope', 'DataService', function($scope, DataService) {

  /*SedeService.getDataLocations().then(function(data) {
    $scope.locations = data;
  });*/

  DataService.getDataLocations().then(function(data) {
    console.log(data);
    $scope.locations = data;
  });

}])

.controller('CorsiCtrl', ['$scope', 'DataService', 'SedeService', function($scope, DataService) {

  DataService.getDataCourse().then(function(data) {
    $scope.courses = data;
  });

}])

.controller('MapSediCtrl', ['$scope', 'SedeService', function($scope, SedeService) {

  SedeService.getMapLocations();
}])

.controller('MapCurrentLocationCtrl', ['$scope', 'SedeService', function($scope, SedeService) {

  SedeService.getCurrentMapLocation()
}])

.controller('MapCtrl', ['$scope', 'DataService', 'SedeService', '$stateParams', function($scope, DataService, SedeService, $stateParams) {

  /*SedeService.getLocationById($stateParams.sedeId).then(function(data) {
    SedeService.getMapLocationById(data);
    $scope.location = data;
  });*/

  DataService.getDataById($stateParams.sedeId, 'locations').then(function(data) {
    SedeService.getMapLocationById(data);
    $scope.location = data;
  });

}])

.controller('SerateCtrl', ['$ionicPlatform', '$scope', '$cordovaSocialSharing', function($ionicPlatform, $scope, $cordovaSocialSharing) {

  $ionicPlatform.ready(function() {

    var message = 'This is a demo message';
    var subject = 'This is a demo message';
    var link = 'http://somerandom.link/image.png'; // fake image

    $scope.nativeShare = function() {
      $cordovaSocialSharing
        .share(message, subject, link); // Share via native share sheet
    };
  });
}])

.controller('AlbumsCtrl', ['$scope', '$ionicLoading', '$state', 'FlickrService', function($scope, $ionicLoading, $state, FlickrService) {

  $ionicLoading.show();
  // Getting Photosets Detail from Flickr Service
  FlickrService.getPhotoSets().then(function(data) {

    if (data.stat != 'fail') {
      $scope.photoList = data.photosets.photoset;
      $ionicLoading.hide();
    } else {
      alert(data.message);
      $ionicLoading.hide();
    }


  });
}])

.controller('GalleryCtrl', ['$scope', '$ionicLoading', '$stateParams', 'FlickrService', function($scope, $ionicLoading, $stateParams, FlickrService) {

  $ionicLoading.show();

  $scope.id = $stateParams.id;
  $scope.photoList = [];

  // Getting List of Photos from a Photoset
  FlickrService.getPhotos($scope.id).then(function(result) {

    $ionicLoading.hide();

    $scope.photos = result.photoset.photo;

    angular.forEach($scope.photos, function(photo, key) {
      var id = photo.id;
      FlickrService.getInfoPhoto(id).then(function(result) {
        $scope.photoList.push({
          sizes: result
        });
      });
    });
  });

}])

.controller('DBCtrl', ['$scope', 'DataService', 'SedeService', function($scope, DataService, SedeService) {

  /*  DataService.getDataLocations().then(function(data) {
        console.log(data);
        $scope.locations = data;
    });

  DataService.getDataById(1,'locations').then(function(data) {
    SedeService.getMapLocationById(data);
    console.log()
    $scope.location = data;
  });
*/

  DataService.getDataCourse().then(function(data) {
    console.log(data);
    $scope.locations = data;
  });

}])
