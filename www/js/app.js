// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic', 'app.controllers', 'app.services', 'app.filter', 'ngCordova'])

.run(function($ionicPlatform,DataService) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    // Instantiate database file/connection after ionic .
    DataService.configData();
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.news', {
    url: '/news',
    views: {
      'tab-news': {
        templateUrl: 'templates/news.html',
      }
    }
  })

  .state('app.news-info', {
    url: '/news-info',
    views: {
      'tab-news': {
        templateUrl: 'templates/news-info.html',
      }
    }
  })

  .state('app.eventi', {
    url: '/eventi',
    views: {
      'tab-eventi': {
        templateUrl: 'templates/eventi.html',

      }
    }
  })


  .state('app.evento-info', {
    url: '/evento-info',
    views: {
      'tab-eventi': {
        templateUrl: 'templates/evento-info.html',

      }
    }
  })

  .state('app.corsi', {
    url: '/corsi',
    views: {
      'tab-corsi': {
        templateUrl: 'templates/corsi.html',
        controller: 'CorsiCtrl'

      }
    }
  })

  .state('app.corso-info', {
    url: '/corso-info',
    views: {
      'tab-corsi': {
        templateUrl: 'templates/corso-info.html',

      }
    }
  })

  .state('app.serate', {
    url: '/serate',
    views: {
      'tab-serate': {
        templateUrl: 'templates/serate.html',
        controller: 'SerateCtrl'
      }
    }
  })

  .state('app.serate-info', {
    url: '/serate-info',
    views: {
      'tab-serate': {
        templateUrl: 'templates/serate-info.html',
      }
    }
  })

  .state('app.sedi', {
    url: '/sedi',
    views: {
      'tab-sedi': {
        templateUrl: 'templates/sedi.html',
        controller: 'SediCtrl'
      }
    }
  })

  .state('app.map-sedi', {
    url: '/map-sedi',
    views: {
      'tab-sedi': {
        templateUrl: 'templates/map-sedi.html',
        controller: 'MapCurrentLocationCtrl'
      }
    }
  })

  .state('app.current-location', {
    url: '/current-location',
    views: {
      'tab-sedi': {
        templateUrl: 'templates/current-location.html',
        controller: 'MapCurrentLocationCtrl'
      }
    }
  })

  .state('app.sede-info', {
    url: '/sede-info/:sedeId',
    views: {
      'tab-sedi': {
        templateUrl: 'templates/sede-info.html',
        controller: 'MapCtrl'
      }
    }
  })

  .state('app.albums', {
    url: '/albums',
    views: {
      'tab-albums': {
        templateUrl: 'templates/albums.html',
        controller: 'AlbumsCtrl'
      }
    }
  })

  .state('app.gallery', {
    url: '/gallery/:id',
    views: {
      'tab-albums': {
        templateUrl: 'templates/gallery.html',
        controller: 'GalleryCtrl'
      }
    }
  })

  .state('app.db', {
    url: '/db',
    views: {
      'tab-albums': {
        templateUrl: 'templates/db.html',
        controller: 'DBCtrl'
      }
    }
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/news');
})
