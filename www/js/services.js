angular.module('app.services', [])

.value('Flickr_data', {
  key: '1c132ffb3557243bd7ee6401e734e488',
  endpoint: 'https://api.flickr.com/services/rest/',
  user_id: '69415591@N05'
})

.factory('SedeService', ['$http', '$q', '$cordovaGeolocation', function($http, $q, $cordovaGeolocation) {


  //Obtener todos los datos de las sedes del file js/sedi.json
  function getDataLocations() {
    var deferred = $q.defer();

    $http.get('js/sedi.json', {
        cache: true
      })
      .success(function(data) {
        deferred.resolve(data);
      });

    return deferred.promise;
  };

  //Obtener los datos de una Sede por su ID
  function getLocationById(id) {

    var deferred = $q.defer();

    getDataLocations().then(function(data) {

      var results = data.filter(function(location) {

        return location.id === id;
      });

      if (results.length > 0) {
        deferred.resolve(results[0]);

      } else {
        deferred.reject();
      }
    });

    return deferred.promise;
  };


  //Definimos el estilo de la Mapa
  function getMapStyles() {

    // Specify features and elements to define styles.
    var stylesMap = [{
      "featureType": "landscape",
      "stylers": [{
        "saturation": -100
      }, {
        "lightness": 65
      }, {
        "visibility": "on"
      }]
    }, {
      "featureType": "poi",
      "stylers": [{
        "saturation": -100
      }, {
        "lightness": 51
      }, {
        "visibility": "simplified"
      }]
    }, {
      "featureType": "road.highway",
      "stylers": [{
        "saturation": -100
      }, {
        "visibility": "simplified"
      }]
    }, {
      "featureType": "road.arterial",
      "stylers": [{
        "saturation": -100
      }, {
        "lightness": 30
      }, {
        "visibility": "on"
      }]
    }, {
      "featureType": "road.local",
      "stylers": [{
        "saturation": -100
      }, {
        "lightness": 40
      }, {
        "visibility": "on"
      }]
    }, {
      "featureType": "transit",
      "stylers": [{
        "saturation": -100
      }, {
        "visibility": "simplified"
      }]
    }, {
      "featureType": "administrative.province",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "water",
      "elementType": "labels",
      "stylers": [{
        "visibility": "on"
      }, {
        "lightness": -25
      }, {
        "saturation": -100
      }]
    }, {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [{
        "hue": "#ffff00"
      }, {
        "lightness": -25
      }, {
        "saturation": -97
      }]
    }];

    return stylesMap;

  };

  // Mostrar todas las sedes en la mapa
  function getMarkersLocations(data) {

    var LatLngCenter = {
      lat: 45.5742961,
      lng: 9.000000
    };
    var stylesMap = getMapStyles();

    var map = new google.maps.Map(document.getElementById('map-sedi-container'), {

      center: LatLngCenter,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      zoomControl: false,
      mapTypeControl: false,
      streetViewControl: false,
      draggable: true,
      styles: stylesMap,
      zoom: 10
    });

    //Muestra los icon de las sede en el mapa
    var image = 'img/location_map.png';
    var infowindow = new google.maps.InfoWindow();

    for (var i = 0; i < data.length; ++i) {

      var content = "<h5 class='style-market-name'>" + data[i].name + "</h5>" + "<p class='style-market-address'>" + data[i].address + "<br />" + data[i].city + "</p>";
      var latlng = new google.maps.LatLng(data[i].lat, data[i].log);
      var marker = new google.maps.Marker({
        map: map,
        animation: google.maps.Animation.BOUNCE,
        position: latlng,
        icon: image,
        title: content
      });

      google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(this.title);
        infowindow.open(map, this);
      });

    }

    return map;

  };


  //Creación de la Mapa con todas las sedes
  function getMapLocations() {

    getDataLocations().then(function(data) {
      getMarkersLocations(data);
    });

  };


  //Creación de la Mapa con todas las sedes
  function getCurrentMapLocation() {

    getDataLocations().then(function(data) {

      var map = getMarkersLocations(data);

      $cordovaGeolocation
        .getCurrentPosition({
          timeout: 10000,
          enableHighAccuracy: false
        })
        .then(function(position) {

          //Muestra nuestra ubicacion  en la mapa
          var icon = 'img/current_location_map.png';
          var myLatLng = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };

          var currentmarker = new google.maps.Marker({
            map: map,
            animation: google.maps.Animation.BOUNCE,
            position: myLatLng,
            icon: icon
          });

        }, function(err) {

          getMapLocations();
        });

    });
  };

  //Creación de la Mapa de una sede por si ID
  function getMapLocationById(data) {

    var sadeLatLng = new google.maps.LatLng(data.lat, data.log);

    var stylesMap = getMapStyles();

    // Create a map object and specify the DOM element for display.
    var map = new google.maps.Map(document.getElementById('map-container'), {
      center: sadeLatLng,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      zoomControl: false,
      mapTypeControl: false,
      streetViewControl: false,
      draggable: true,
      styles: stylesMap,
      zoom: 11
    });

    var image = 'img/location_map.png';
    var infowindow = new google.maps.InfoWindow();
    var content = "<h5 class='style-market-name'>" + data.name + "</h5>" + "<p class='style-market-address'>" + data.address + "<br />" + data.city + "</p>";

    var marker = new google.maps.Marker({
      map: map,
      animation: google.maps.Animation.BOUNCE,
      position: sadeLatLng,
      icon: image,
      title: content
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.setContent(this.title);
      infowindow.open(map, this);
    });

  };

  return {

    getDataLocations: getDataLocations,
    getLocationById: getLocationById,
    getMapLocations: getMapLocations,
    getMapLocationById: getMapLocationById,
    getCurrentMapLocation: getCurrentMapLocation

  };

}])

.factory('FlickrService', ['$http', '$q', 'Flickr_data', function($http, $q, Flickr_data) {

  // Getting List of Photoset in a user account.
  function getPhotoSets() {

    var deferred = $q.defer();

    var url = Flickr_data.endpoint +
      '?method=flickr.photosets.getList&api_key=' + Flickr_data.key +
      '&user_id=' + Flickr_data.user_id +
      '&format=json&nojsoncallback=1';

    $http.get(url, {
        cache: true
      })
      .success(function(data) {
        deferred.resolve(data);
      })
      .error(function(error) {
        console.log(data);
      });

    return deferred.promise;
  };

  // Getting Photos of a photo set
  function getPhotos(photoset_id) {

    var deferred = $q.defer();

    var url = Flickr_data.endpoint +
      '?method=flickr.photosets.getPhotos&api_key=' + Flickr_data.key +
      '&user_id=' + Flickr_data.user_id +
      '&photoset_id=' + photoset_id +
      '&format=json&nojsoncallback=1';

    $http.get(url, {
        cache: true
      })
      .success(function(data) {
        console.log(url);
        deferred.resolve(data);
      });

    return deferred.promise;
  };

  // Getting Info for each photo.
  function getInfoPhoto(id) {

    var deferred = $q.defer();

    var sizes = Flickr_data.endpoint +
      '?method=flickr.photos.getSizes&api_key=' + Flickr_data.key +
      '&photo_id=' + id + '&format=json&nojsoncallback=1';

    $http.get(sizes, {
        cache: true
      })
      .success(function(data) {
        deferred.resolve(data);
      });

    return deferred.promise;
  };

  return {
    getPhotoSets: getPhotoSets,
    getPhotos: getPhotos,
    getInfoPhoto: getInfoPhoto
  };

}])

.factory('DataService', ['$http', '$q', '$cordovaSQLite', function($http, $q, $cordovaSQLite) {

  var db = {};
  var dataLocations = [];
  var dataCourses = [];

  function configData() {

    // Instantiate database file/connection after ionic .
    db = $cordovaSQLite.openDB("joker-dance.db", 1);

    createTableLocations() ;

    createTableCourses();



  };


  //Crendo la tabla locations con los datos
  function createTableLocations() {

    $cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS locations').then(function(res) {

      $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS locations ( id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name TEXT, address TEXT, city	TEXT, desc	TEXT, lat	TEXT NOT NULL, log	TEXT NOT NULL, tel	TEXT, img	TEXT, timestamp	TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)");
      $cordovaSQLite.execute(db, "INSERT INTO locations (id,name,address,city,desc,lat,log,tel,img,timestamp) VALUES (1,'Parabiago','Via Re Depaolini, 68','20015 Parabiago MI','Sede Principale','45.546282','8.959073','+39 338 317 9645','img/FfkUSSuTjW8s3hOLCUXa_Carnevale.jpg','2016-03-07 23:05:08')");
      $cordovaSQLite.execute(db, "INSERT INTO locations (id,name,address,city,desc,lat,log,tel,img,timestamp) VALUES (2,'Legnano','Via Barbara Melzi, 20','20025 Legnano MI','Centro Giovanile Legnarello','45.599433','8.923734','+39 338 317 9645','img/FfkUSSuTjW8s3hOLCUXa_Carnevale.jpg','2016-03-07 23:07:50')");
      $cordovaSQLite.execute(db, "INSERT INTO locations (id,name,address,city,desc,lat,log,tel,img,timestamp) VALUES (3,'Canegrate','Via Redipuglia','20010 Canegrate MI','Palestra Scuola Elementare','45.566182','8.929205','+39 338 317 9645','img/FfkUSSuTjW8s3hOLCUXa_Carnevale.jpg','2016-03-07 23:09:17')");
      $cordovaSQLite.execute(db, "INSERT INTO locations (id,name,address,city,desc,lat,log,tel,img,timestamp) VALUES (4,'Pogliano Milanese','Via G. Garibaldi','20010 Pogliano Milanese MI','Palestra Scuola Media','45.534066','8.990862','+39 338 317 9645','img/FfkUSSuTjW8s3hOLCUXa_Carnevale.jpg','2016-03-07 23:10:54')");
      $cordovaSQLite.execute(db, "INSERT INTO locations (id,name,address,city,desc,lat,log,tel,img,timestamp) VALUES (5,'Garbagnate Milanese','Via Gran Sasso, 1','20024 Garbagnate Milanese MI','Oratorio S. Luigi','45.574897','9.078060','+39 338 317 964','img/FfkUSSuTjW8s3hOLCUXa_Carnevale.jpg','2016-03-07 23:12:17')");

    }, function(err) {
      console.error(err);
    });

  };

  //Crendo la tabla Course con los datos
  function createTableCourses() {

    $cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS courses').then(function(res) {

      $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS courses ( id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name	TEXT, desc	TEXT, img	TEXT, video	TEXT, icon TEXT,timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)");
      $cordovaSQLite.execute(db, "INSERT INTO courses VALUES (1,'Ballo Liscio','<p>Il ballo liscio e’ una disciplina tipicamente di coppia e si può suddividere in due categorie ben distinte.</p><p><strong>Liscio Unificato</strong> disciplina composta da tre balli: <strong>Mazurka</strong>, <strong>Valzer Viennese</strong>, <strong>Polka</strong>.</p><p>Si tratta di una disciplina tipicamente italiana, non presente negli altri paesi. Anche se nessuno di questi balli è nato in Italia, possiamo trovare una nostra tradizione culturale e musicale data dalla lunga pratica da parte delle nostre popolazioni.</p><p>Il ballo liscio sarebbe quindi sinonimo di ballo scorrevole, contrapposto sia a quello saltato sia a quello camminato. Riferito alla musica, il ritmo liscio è quello che si differenzia dal ritmo sincopato.</p><p><strong>Ballo da Sala</strong> disciplina composta da tre balli: <strong>Valzer Lento</strong>, <strong>Tango</strong> e <strong>Fox Trot</strong>.</p><p>Balli gradevoli dal punto di vista estetico e presentano dei vantaggi di tipo pratico per chi li usa a fini di divertimento e di ginnastica, nelle balere e nelle sale da ballo.</p><p>I grandi atleti, i competitori, i campioni sono inevitabilmente creatori di stili e di variazioni di figure.</p>',NULL,'wMDGvyuy0R0','img/icon_courses/liscio.png','2016-03-09 11:44:30')");
      $cordovaSQLite.execute(db, "INSERT INTO courses VALUES (2,'Danze Caraibiche','<p>Le <strong>Danze Caraibiche</strong>, <strong>Salsa</strong> e <strong>Bachata</strong>, sono originarie del centro America, le cui isole madri sono Cuba, la Repubblica Domenicana e Puerto Rico e hanno origini che risalgono ai secoli scorsi ma sono diventate popolarissime solo in questi ultimi anni.<br>Sono balli che si caratterizzano dagli altri balli per l’<strong>allegria</strong>, il <strong>ritmo frizzante</strong>, la <strong>vivacita’</strong> e la<strong>sensualita’</strong>.</p><p>La <strong>Salsa</strong> è un ballo che esalta la tecnica con l’eleganza. Salsa portoricana vuol dire esprimere la bellezza delle movenze, l’armonia dell’intesa perfetta tra te e il partner.<br>La salsa portoricana è una perfetta intesa nella coppia che vola su un ritmo recitando l’emozione di un incontro.</p><p>La <strong>Bachata</strong> è una Danza sudamericana affine al bolero, che si balla in coppia. Tra i generi caraibici è uno dei pochi in cui l’influenza dei ritmi africani è meno evidente, difatti la musica presenta un suono dolce e melodico e i testi delle canzoni trattano sempre il tema dell’amore in tutte le sue sfumature.<br>E’ un ballo in cui l’uomo e la donna restano abbracciati dondolandosi ed effettuando un provocatorio movimento d’anca sul quarto battito musicale.</p>',NULL,'_6OIsgZ7FXg','img/icon_courses/caraibico.png','2016-03-09 11:48:33')");
      $cordovaSQLite.execute(db, "INSERT INTO courses VALUES (3,'Tango Argentino','<p>Il <strong>Tango Argentino</strong> è caratterizzato da tre ritmi musicali diversi, <strong>Tango</strong>, <strong>Milonga</strong> e <strong>Tango Vals</strong>, ai quali corrispondono altrettante tipologie di ballo.</p><p>Il tango argentino è una forma d’arte che comprende musica e danza nata a Buenos Aires, Argentina intorno alla seconda metà dell’800, nei quartieri poveri ed emarginati. Pur essendo una musica molto sincopata, non utilizza strumenti a percussione ed anche gli altri strumenti utilizzati vengono suonati in modo del tutto particolare per dare forti accenti di battuta e segnature ritmiche. La sua struttura armonica, però, è tipicamente italiana.</p><p>E’ un ballo basato sull’<strong>improvvisazione</strong> e si distingue dagli altri per la maggior <strong>eleganza</strong> e<strong>passionalità</strong>. Il passo base del tango è il passo in sè, dove per passo si intende il normale passo di una camminata. Essendo un ballo di improvvisazione in pista non esiste l’idea di sequenze di passi predefiniti, e sta alla fantasia dei ballerini costruire come in un dialogo il proprio ballo con il proprio partner.</p><p>La posizione di ballo è un abbraccio frontale asimmetrico, in cui l’uomo con la destra cinge la schiena della propria ballerina e con la sinistra le tiene la mano, che sta a significare la relazione tra l’uomo e la donna. Il tango è un ballo essenzialmente di “comunicazione”.</p>',NULL,NULL,'img/icon_courses/tango.png','2016-03-09 16:06:11')");
      $cordovaSQLite.execute(db, "INSERT INTO courses VALUES (4,'Bachata','<p>La <strong>Bachata</strong> è un genere musicale originario della Repubblica Dominicana che ha dato origine al relativo <strong>ballo di coppia</strong>.</p><p>Tra i generi caraibici è uno dei pochi in cui l’influenza dei ritmi africani è meno evidente, difatti la musica presenta un<strong> suono dolce e melodico</strong> e i testi delle canzoni trattano sempre il tema dell’amore in tutte le sue sfumature.</p><p>Ha origini intorno gli anni 40, ma era diffusa solamente nelle classi sociali più povere della Repubblica Dominicana.</p><p>La Bachata delle origini non presentava molte figure come nell’interpretazione moderna; era un ballo in cui l’uomo e la donna, per tutta la durata del brano, restavano abbracciati dondolandosi ed effettuando un provocatorio movimento d’anca sul quarto battito musicale.</p>',NULL,NULL,'img/icon_courses/bachata.png','2016-03-09 16:09:11')");
      $cordovaSQLite.execute(db, "INSERT INTO courses VALUES (5,'Bachatango','<p>La <strong>Bachatango</strong> e’ una danza di origine recente nata dalla fusione della <strong>Bachata</strong> con il <strong>Tango</strong>.</p><p>La facilita’ di accostamento con altri stili o generi musicale deriva dal fatto che la musica della bachatango e’ suonata in un tempo 4/4, cosi’ come altri generi latino americani.</p>',NULL,NULL,'img/icon_courses/bachatango.png','2016-03-09 16:10:24')");
      $cordovaSQLite.execute(db, "INSERT INTO courses VALUES (6,'Kizomba','<p>La <strong>Kizomba</strong> è un ritmo africano, diffuso nei paesi di lingua portoghese come Capo Verde, con ritmi angolani antichi, suoni elettronici e altri strumenti di percussione.</p><p><strong>Estremamente sensuale</strong> e ballata molto vicino tra il corpo dell’uomo e della donna si crea una complicità, in quanto alcuni movimenti sono molto lenti e insistenti.<br>Richiede una <strong>flessibilità delle ginocchia</strong> alternando un su e giù e del bacino.</p><p>Le danze che più l’hanno influenzata: il Semba (predecessore del Samba), lo Zouk (proveniente dalle Antille) Coladeira, Colà-Zouk, Maringa, Kabetula, Kazukuta, Caduque e Rebita, senza dimenticare la grande scuola del tango e del Merengue, che hanno contribuito alla ricchezza del ballo.</p>',NULL,'mCwQOMPD3GI','img/icon_courses/kizomba.png','2016-03-09 16:11:27')");
      $cordovaSQLite.execute(db, "INSERT INTO courses VALUES (7,'Rueda de Casino','<p>La <strong>Rueda de Casino</strong> e’ una particolare versione della salsa cubana, ballata da piu’ coppie che si dispongono a <strong>formare un grande cerchio</strong>, dove le figure vengono “chiamate” da uno dei ballerini che ha la funzione di capo rueda.</p><p>In generale, la Rueda è più <strong>scanzonata</strong> e <strong>meno tecnica</strong> del ballo in coppia singola: molte figure sono infatti sberleffi, prese in giro, allusioni sessuali, finte punizioni e amenità varie, realizzate tra le risate generali di tutti i ballerini.</p><p>La caratteristica principale della rueda e’ di <strong>cambiare continuamente partner</strong> durante il brano: le coppie si smembrano e si riformano in continuazione, dando in tal modo la possibilita’ di far ballare tutti gli uomini con tutte le donne.</p>',NULL,NULL,'img/icon_courses/ruedadecasino.png','2016-03-09 16:14:06')");
      $cordovaSQLite.execute(db, "INSERT INTO courses VALUES (8,'Boogie Woogie','<p>Il<strong> boogie woogie</strong> e’ un evoluzione dello <strong>swing</strong>, caratterizzato da un’esplosione di <strong>volteggi</strong> e il <strong>continuo allontanarsi e riavvicinarsi dei partners</strong>, da acrobazie dalle piu’ semplici alle piu’ ardue, simile a quelle del <strong>rock&amp;roll</strong>, mantenendo pero’ il bagaglio di figure dello <strong>jive</strong>.</p><p>Il boogie woogie richiede una buona dose di ritmo istintivo nelle gambe ed insegna alla coppia il giusto ed equilibrato senso d’esibizionismo nella sua essenza positiva.</p>',NULL,NULL,'img/icon_courses/boogiewoogie.png','2016-03-09 16:15:07')");
      $cordovaSQLite.execute(db, "INSERT INTO courses VALUES (9,'Latino Americano','<p>I <strong>balli latino americani</strong> sono balli provenienti dal Sud America e sono molto diffusi. Infatti si prestano ad originali interpretazioni e <strong>fantasia creativa</strong> con <strong>ritmi incalzanti e sensuali</strong>.<br>Chi ama la danza e chi ama ballare sicuramente conoscerà i vari stili di ballo latino americani in quanto ritenuti da tutti i balli di coppia maggiormente indicati per favorire un <strong>benessere fisico e mentale</strong>.</p><p><strong>SAMBA:</strong> particolare movimento del bacino con figure di notevole pregio coreografico</p><p><strong>CHA-CHA-CHA:</strong> consiste in una serie di tre passi con tempi diversi, lo chasse’</p><p><strong>RUMBA:</strong> basato su movimenti seducenti dei fianchi e del bacino</p><p><strong>PASO DOBLE:</strong> ispirato alle azioni del torero nella lotta contro il toro</p><p><strong>JIVE:</strong> dal ritmo frenetico, le sue figure richiedono stile e resistenza fisica</p><p>Questi cinque diversi balli esprimono ciascuno sentimenti diversi che possono essere rappresentati grazie al feeling presente tra i due ballerini, componente fondamentale per questi tipi di ballo.<br>Le figure relative a questi balli differiscono da continente a continente e anche da una nazione all’ altra.</p>',NULL,NULL,'img/icon_courses/latinoamericano.png','2016-03-09 16:16:33')");
      $cordovaSQLite.execute(db, "INSERT INTO courses VALUES (10,'Country','<p>Il ballo <strong>country</strong> e’ composto da semplici coreografie, un ballo estremamente <strong>coinvolgente</strong> che consente di trascorrere divertenti serate in compagnia, da ballare <strong>in coppia o in gruppo</strong>.</p><p>I balli country sono balli irlandesi e americani, e dall’america sono stati esportati in tutta europa. Ogni ballo ha la sua coreografia che puo’ essere eseguita in line-dance, dove i ballerini si dispongono in linee orizzontali e verticali, oppure in partner-dance, dove i ballerini sono in coppia ed è possibile eseguire la coreografia in linea oppure in cerchio.</p><p>Le musiche e le coreografie country raccontano la vita quotidiana, le origini e il modo di essere dei cowboy.</p><p><strong>Se vuoi entrare meglio nella parte, mettiti stivali, jeans, maglietta e cappello!</strong></p>',NULL,'3nGtqcEjU18','img/icon_courses/country.png','2016-03-09 16:17:34')");
      $cordovaSQLite.execute(db, "INSERT INTO courses VALUES (11,'Danza moderna','<p><strong>Danza moderna</strong> è sinonimo di <strong>Fluidità</strong> e <strong>Stile</strong>. Con questa Danza si torna a dare importanza al gesto e all’espressività, uscendo dalla rigidità dei canoni classici, esplorando molto di più le possibilità di movimento del proprio corpo.</p><p>Al ballerino viene permesso di evadere, scoprire e inventare nuovi modi di vivere la danza, fin da bambini, attraverso l’improvvisazione, il gioco e la scoperta, durante il percorso di studio di stili tecnici molto diversi e particolari.</p><p>Danza moderna infatti comprende lo stile più <strong>jazz</strong>, <strong>televisivo</strong> e <strong>Broadway musical</strong>, quello più lirical e morbido, o più pop-funky, senza dimenticare il divertimento, il piacere di stare in forma e di stare in compagnia.</p>',NULL,NULL,'img/icon_courses/danzamoderna.png','2016-03-09 16:18:58')");
      $cordovaSQLite.execute(db, "INSERT INTO courses VALUES (12,'Danza del Ventre','<p>La <strong>Danza del Ventre</strong> è caratterizzata da <strong>movimenti sinuosi</strong>, <strong>sensualità</strong> e grazia ritmica dei movimenti.</p><p>Viene praticata, anche a scopi terapeutici, non solo per donare al corpo grazia e scioltezza, ma anche per mantenere un peso ideale.<br>Aumenta la flessibilità e la tonicità del seno, delle spalle, delle braccia, del bacino, ma soprattutto della pancia: gli <strong>addominali</strong> sono coinvolti profondamente nei movimenti, modellando la linea e giovando agli organi interni. <strong>Tonifica le cosce</strong>, ottimizza l’<strong>agilita’ delle articolazioni</strong> e probabilmente ritarda l’osteoporosi.</p><p>I suoi <strong>benefici psicofisici</strong>, aiutano a migliorare la circolazione sanguigna, attenuare i dolori mestruali e quelli della colonna vertebrale, sia a livello lombare che cervicale, mentre a livello psicologico i vantaggi ottenibili sono stati spesso individuati in termini di rilascio delle tensioni, di un senso di rinascita e di riscoperta della femminilità.</p>',NULL,NULL,'img/icon_courses/danzadelventre.png','2016-03-09 16:20:23')");
      $cordovaSQLite.execute(db, "INSERT INTO courses VALUES (13,'Zumba','<p>Sei pronto a <strong>divertirti</strong> e a <strong>tenerti in forma</strong>?<br>Il programma <strong>Zumba</strong> non è altro che questo!</p><p>Zumba infatti, si basa sul principio che fare esercizio deve essere piacevole e divertente prima di tutto, questo aiuta a seguire il programma fino al raggiungimento dei risultati voluti.</p><p>Seguendo le <strong>semplici coreografie non-stop</strong>, puoi bruciare da 350 a 1000 calorie per lezione e tonificare il tuo corpo dalla testa ai piedi. E’ <strong>fitness</strong> e <strong>danza brucia calorie</strong>, <strong>divertentissimo</strong>,<strong>efficace</strong>, <strong>facile da seguire</strong>, ispirato ai ritmi latino americani, che sta guidando milioni di persone verso la gioia e la salute.</p><p>A volte le grandi idee nascono per caso. Nella metà degli anni ’90, nessuno avrebbe potuto prevedere il successo del Programma Zumba, neanche lo stesso ideatore Alberto “Beto” Perez.</p><p>Oggi Zumba viene insegnato in palestre e studi in tutto il mondo a milioni di appassionati. In questo modo si è creato spontaneamente un nuovo tipo di fitness-danza, che si basa sull’abbandono dei movimenti.</p>',NULL,'dVMPqdIgKNU','img/icon_courses/zumba.png','2016-03-09 16:21:23')");
      $cordovaSQLite.execute(db, "INSERT INTO courses VALUES (14,'Pilates','<p>Il <strong>Pilates</strong> permette di acquisire <strong>consapevolezza del respiro</strong> e dell’<strong>allineamento della colonna vertebrale</strong> <strong>rinforzando i muscoli</strong> del piano profondo del tronco, molto importanti per <strong>aiutare ad alleviare e prevenire mal di schiena</strong>.</p><p>Con questo metodo di allenamento coinvolgeremo:<strong> l’addome, i glutei, gli adduttori e la zona lombare</strong>.</p>',NULL,NULL,'img/icon_courses/pilates.png','2016-03-09 16:23:59')");
      $cordovaSQLite.execute(db, "INSERT INTO courses VALUES (15,'Balli di Gruppo','<p>I <strong>balli di gruppo</strong> sono stati concepiti per <strong>divertire</strong> e <strong>stare insieme in allegria!</strong><br>La danza infatti, in particolare quella corale, fatta in gruppo, è un importante momento di <strong>socialità</strong>, oltre che di <strong>relax</strong> e <strong>divertimento</strong>.</p><p>Ballando in gruppo ci si può liberare, lasciarsi andare, rapportarsi in modo nuovo e diverso con gli altri e trovare un nostro equilibrio come individui e come parte del gruppo.</p>',NULL,'DpX95GflJDY','img/icon_courses/gruppo.png','2016-03-09 16:25:31')");

    }, function(err) {
      console.error(err);
    });

  };

  //Obtener todos los datos de las sedes del DB Table locations
  function getDataLocations() {

    var deferred = $q.defer();
    var query = "SELECT id,name,address,city,img FROM locations";

    $cordovaSQLite.execute(db, query).then(function(res) {

      for (var j = 0; j < res.rows.length; j++) {
        dataLocations.push(res.rows.item(j));
      }

      deferred.resolve(dataLocations);

    }, function(err) {
      deferred.reject(err);
    });

    return deferred.promise;
  };

  //Obtener los datos de una Sede por su ID
  function getDataById($locationId, $table) {

    var deferred = $q.defer();
    var query = "SELECT * FROM " + $table + " WHERE id = (?)";

    $cordovaSQLite.execute(db, query, [$locationId]).then(function(res) {

      deferred.resolve(res.rows.item(0));

    }, function(err) {
      console.error(err);
      deferred.reject(err);
    });

    return deferred.promise;
  };

  //Obtener todos los datos de los Cursos del DB Table courses
  function getDataCourse() {

    var deferred = $q.defer();
    var query = "SELECT id,name,img,icon FROM courses";

    $cordovaSQLite.execute(db, query).then(function(res) {

      for (var j = 0; j < res.rows.length; j++) {
        dataCourses.push(res.rows.item(j));
      }

      deferred.resolve(dataCourses);

    }, function(err) {
      deferred.reject(err);
    });

    return deferred.promise;
  };


  return {
    configData: configData,
    getDataLocations: getDataLocations,
    getDataById: getDataById,
    getDataCourse: getDataCourse
  }
}])
